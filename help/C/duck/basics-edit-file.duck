= Editing files
  [topic]
@link[guide >index#basics]

@revision[version=0.1 date=2022-01-04 status=incomplete]

@credit[author copyright]
  @name Logan Rathbone
  @email poprocks@gmail.com
  @years 2022

== Toggling the hex or ASCII panes

File can be edited in GHex from either the hex pane or the ASCII pane.

To focus either pane to accept your keystrokes, you may click on the appropriate pane, or:
* Press $keyseq($key(Alt)$key(Left)) to focus the hex pane, and
* Press $keyseq($key(Alt)$key(Right)) to focus the ASCII pane.

Use any of the following methods to move the cursor around the file:
* Use the scrollbar on the window and the mouse to select a byte by clicking on it.
* Use the arrow keys, $key(Home), $key(End), $key(PageUp) or $key(PageDown) on the keyboard.
* Choose $gui(Jump to Byte) from the $(Main menu) or press $keyseq($key(Ctrl)$key(J)),
  and enter the cursor offset as a decimal or hexadecimal value.
  The format of the hexadecimal value must be 0x followed by the offset, for example 0x3.

== Editing the file using the hex or ASCII pane

Edit the ASCII format in the same way you edit a normal text file.

To edit the hexadecimal format, use the values 0 to 9 and a to f. This
operation is not case-sensitive.

== Insert mode

$gui(Insert Mode) can be toggled on or off by pressing $key(Insert), or by
clicking the $gui(Toggle insert mode) button in the status bar.

When $gui(Insert Mode) is enabled, this means data will be added to the file as
opposed to replacing existing data.

Unlike traditional text editors, having $gui(Insert Mode) $em(off) is
considered the default state in GHex, given that hex editors are routinely used
to edit existing data in binary files that are very specifically laid out. In
some cases, such files may be of a fixed size and increasing or decreasing
their size may cause undefined or unwanted behaviour in applications that open
them.
